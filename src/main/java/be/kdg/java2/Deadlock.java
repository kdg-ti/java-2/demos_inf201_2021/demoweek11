package be.kdg.java2;

import java.util.Random;

public class Deadlock {
    public static final String LOCK1 = "lock1";
    public static final String LOCK2 = "lock2";

    static class Task implements Runnable {
        private String lock1;
        private String lock2;

        public Task(String lock1, String lock2) {
            this.lock1 = lock1;
            this.lock2 = lock2;
        }

        @Override
        public void run() {
            while (true) {
                System.out.println(Thread.currentThread().getName() + " needs " + lock1);
//                try {
//                    Thread.sleep(new Random().nextInt(100));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                synchronized (lock1) {
                    System.out.println(Thread.currentThread().getName() + " has " + lock1);
                    System.out.println(Thread.currentThread().getName() + " needs " + lock2);
                    synchronized (lock2) {
                        System.out.println(Thread.currentThread().getName() + " has " + lock2);
                        System.out.println(Thread.currentThread().getName() + " is working!");
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Task(LOCK1, LOCK2));
        Thread thread2 = new Thread(new Task(LOCK2, LOCK1));
        thread1.start();
        thread2.start();
    }
}
