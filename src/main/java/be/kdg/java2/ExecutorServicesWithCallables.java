package be.kdg.java2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ExecutorServicesWithCallables {
    static class Task implements Callable<Long> {
        private List<Integer> getallen;
        private int begin, end;

        public Task(List<Integer> getallen, int begin, int end) {
            this.getallen = getallen;
            this.begin = begin;
            this.end = end;
        }

        @Override
        public Long call() throws Exception {
            long result = 0;
            for (int i = begin; i < end; i++) {
                result += getallen.get(i);
            }
            return result;
        }
    }

    public static void main(String[] args) {
        List<Integer> getallen =
                IntStream.range(0, 1000000).boxed()
                        .collect(Collectors.toList());
        ExecutorService executorService
                = Executors.newFixedThreadPool(100);
        List<Future<Long>> futureResultList = new ArrayList<>();
        for (int i = 0; i < getallen.size(); i += 1000) {
            Future<Long> futureResult
                    = executorService.submit(new Task(getallen, i, i + 1000));
            futureResultList.add(futureResult);
        }
        try {
            long totalSum = 0;
            for (Future<Long> longFuture : futureResultList) {
                long result = longFuture.get();
                totalSum += result;
            }
            System.out.println("Totaal:" + totalSum);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
