package be.kdg.java2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorServices {
    static class Task implements Runnable {

        @Override
        public void run() {
            try {
                while (true) {
                    System.out.println(Thread.currentThread().getName()
                            + " greets you");
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (InterruptedException e) {
                System.out.println("Thread " + Thread.currentThread().getName() + " killed...");
                //e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService
                = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 100; i++) {
            executorService.execute(new Task());
        }
        executorService.execute(() -> {
            System.out.println("Dit moest ik ook nog doen");
        });
        System.out.println("TRYING TO SHUTDOWN!!!!");
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

