package be.kdg.java2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadlockSolved {
    public static final Lock LOCK1 = new ReentrantLock();
    public static final Lock LOCK2 = new ReentrantLock();

    static class Task implements Runnable {
        private Lock lock1;
        private Lock lock2;

        public Task(Lock lock1, Lock lock2) {
            this.lock1 = lock1;
            this.lock2 = lock2;
        }

        @Override
        public void run() {
            while (true) {
                System.out.println(Thread.currentThread().getName() + " needs " + lock1);
//                try {
//                    Thread.sleep(new Random().nextInt(100));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                //synchronized (lock1) {
                if (lock1.tryLock()) {
                    try {
                        System.out.println(Thread.currentThread().getName() + " has " + lock1);
                        System.out.println(Thread.currentThread().getName() + " needs " + lock2);
                        //synchronized (lock2) {
                        if (lock2.tryLock()) {
                            try {
                                System.out.println(Thread.currentThread().getName() + " has " + lock2);
                                System.out.println(Thread.currentThread().getName() + " is working!");
                            } finally {
                                lock2.unlock();
                            }
                        }
                    } finally {
                        lock1.unlock();
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Task(LOCK1, LOCK2));
        Thread thread2 = new Thread(new Task(LOCK2, LOCK1));
        thread1.start();
        thread2.start();
    }
}
